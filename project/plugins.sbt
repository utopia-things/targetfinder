addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.0.0-RC1")
addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")
addSbtPlugin("com.github.shmishleniy" % "sbt-deploy-ssh" % "0.1.3")
resolvers += "JAnalyse Repository" at "http://www.janalyse.fr/repository/"
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.0-M8")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.16")
