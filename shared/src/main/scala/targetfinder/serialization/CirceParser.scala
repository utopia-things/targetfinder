package targetfinder.serialization

import io.circe._
import io.circe.generic.JsonCodec

import scala.beans.BeanProperty
import io.circe.optics.JsonPath._
import io.circe.parser._

case class Tick(
    tick: Int,
    tickPartial: Int,
    time: Long
)

case class Province(
    loc: String,
    land: Int,
    name: String,
    `protected`: Boolean,
    race: String,
    online: Boolean,
    honor: String,
    nw: Int
)

@JsonCodec case class Kingdom(
    count: Int,
    loc: String,
    provinces: Seq[Province],
    land: Int,
    name: String,
    stance: Stance,
    wars: Seq[Int],
    honor: Int,
    nw: Int
)
case class Stance(
    stance: String,
    towards: Option[String] = Option.empty
)

/**
  * Stance is a little bizarre, it is a string or an array when at war
  */
object Stance {
  implicit val encodeHonor: Encoder[Stance] = (stance: Stance) => {
    stance.towards.fold(Json.fromString(stance.stance)) { towards =>
      Json.fromValues(Seq(Json.fromString(stance.stance), Json.fromString(towards)))
    }
  }

  implicit val decodeHonor: Decoder[Stance] = (c: HCursor) => {
    val optionalStance: Option[Stance] = c.focus.flatMap { json =>
      if (json.isString) {
        json.asString.map(Stance(_))
      } else {
        json.asArray.flatMap { v =>
          Option(Stance(v(0).asString.getOrElse(""), v(1).asString))
        }
      }
    }
    optionalStance.map(Right(_)).getOrElse(Left(DecodingFailure("Not implemented yet", c.history)))
  }
}

object Province {
  def apply(
      loc: String,
      land: Int,
      name: String,
      `protected`: Boolean,
      race: String,
      honor: String,
      nw: Int
  ): Province = new Province(loc, land, name, `protected`, race, false, honor, nw)
  implicit val decodeUser: Decoder[Province] =
    Decoder.forProduct7("loc", "land", "name", "protected", "race", "honor", "nw")(Province.apply)

  implicit val encodeProvince: Encoder[Province] =
    Encoder.forProduct7("loc", "land", "name", "protected", "race", "honor", "nw")(p => (p.loc, p.land, p.name, p.`protected`, p.race, p.honor, p.nw))
}

case class KingdomData(
    @BeanProperty provCount: Int,
    @BeanProperty land: Int,
    @BeanProperty name: String,
    @BeanProperty stance: String,
    @BeanProperty warsWon: Int,
    @BeanProperty warsCount: Int,
    @BeanProperty honor: Int,
    @BeanProperty nw: Int
)
case class ProvinceEvent(
    @BeanProperty utime: String,
    @BeanProperty tick: Int,
    @BeanProperty partialTick: Int,
    @BeanProperty loc: String,
    @BeanProperty province: String,
    @BeanProperty land: Int,
    @BeanProperty nw: Int,
    @BeanProperty kd: KingdomData
)
object ProvinceEvent {
  def apply(tick: (Int, Int, String), kd: Kingdom, prov: Province): ProvinceEvent =
    ProvinceEvent(
      tick._3,
      tick._1,
      tick._2,
      kd.loc,
      prov.name,
      prov.land,
      prov.nw,
      KingdomData(
        kd.count,
        kd.land,
        kd.name,
        kd.stance.stance.toLowerCase(),
        kd.wars(0),
        kd.wars(1),
        kd.honor,
        kd.nw
      )
    )
}

trait CirceParser {
  val kds = root.each.filter(!_.isString).as[Kingdom]

  def provsLike(s: String)(implicit json: Json) =
    root.each.filter(!_.isString).provinces.each.name.string.getAll(json).filter(_.toLowerCase.contains(s.toLowerCase()))

  def provFor(s: String)(implicit json: Json) =
    root.each
      .filter(!_.isString)
      .provinces
      .each
      .filter { j =>
        root.name.string.exist(_.equals(s))(j)
      }
      .as[Province]
      .headOption(json)

  def kdFor(s: String)(implicit json: Json) =
    root.each
      .filter(!_.isString)
      .filter { j =>
        root.provinces.each.name.string.exist(_.equals(s))(j)
      }
      .as[Kingdom]
      .getAll(json)

  def allKds(implicit json: Json) = root.each.filter(!_.isString).as[Kingdom].getAll(json)

  def allProvinces(implicit json: Json): List[(Kingdom, Province)] = allKds.flatMap(kd => kd.provinces.map(p => (kd, p)))

  def jsonFromString(s: String) = parse(s).toOption
}
