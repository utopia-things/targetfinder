package targetfinder.js


import io.circe.Json
import targetfinder.serialization.CirceParser

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("JSApi")
class JSApi(jsonString: String) {
  object encoder extends CirceParser
  import encoder._
  import io.circe.syntax._

  implicit val json = encoder.jsonFromString(jsonString).get

  import JSDictionaries._

  @JSExport()
  def provsLike(s: String)(handler: js.Function1[String, Unit]) = encoder.provsLike(s).foreach(handler)

  @JSExport()
  def provFor(s: String)(handler: js.Function1[Any, Unit]) = encoder.provFor(s).foreach { prov =>
    handler(prov.asObject)
  }

  @JSExport()
  def kdFor(s: String)(handler: js.Function1[Any, Unit]) = encoder.kdFor(s).foreach { kd =>
    handler(kd.asObject)
  }

  @JSExport()
  def kds(handler: js.Function1[Any, Unit]) = encoder.allKds.foreach { kd =>
    handler(kd.asObject)
  }

  var provincesCache: js.Array[(js.Object, js.Object)] = new js.Array[(js.Object, js.Object)]

  @JSExport()
  def provinces(handler: js.Function2[Any, Any, Unit]) =
    if (provincesCache.length == 0) {
      encoder.allProvinces.foreach {
        case (kd, prov) =>
          val kdObj   = kd.asOnlyObject
          val provObj = prov.asObject
          provincesCache.push((kdObj, provObj))
          handler(kdObj, provObj)
      }
    } else {
      var idx = 0;
      while (idx < provincesCache.length) {
        handler(provincesCache(idx)._1, provincesCache(idx)._2)
        idx = idx + 1
      }
    }

}
