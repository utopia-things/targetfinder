package targetfinder.js

import targetfinder.serialization.{Kingdom, Province, Stance}
import scala.scalajs.js
import js.JSConverters._
import scala.scalajs.js

/*
import shapeless._
import labelled.FieldType
import record._



import scala.collection.mutable

object JSObject {
  trait ToMapRec[L <: HList] { def apply(l: L): js.Dictionary[Any] }

  trait LowPriorityToMapRec {
    implicit def hconsToMapRec1[K <: Symbol, V, T <: HList](implicit
                                                            wit: Witness.Aux[K],
                                                            tmrT: Lazy[ToMapRec[T]]
                                                           ): ToMapRec[FieldType[K, V] :: T] = new ToMapRec[FieldType[K, V] :: T] {
      def apply(l: FieldType[K, V] :: T): js.Dictionary[Any] = {
        val tail: mutable.Map[String, Any] = tmrT.value(l.tail)
        tail + (wit.value.name -> l.head)
      }.toJSDictionary
    }

  }

  object ToMapRec extends LowPriorityToMapRec {
    implicit val hnilToMapRec: ToMapRec[HNil] = new ToMapRec[HNil] {
      def apply(l: HNil): js.Dictionary[Any] = {
        js.Dictionary.empty
      }
    }

    implicit def hconsToMapRecSeq0[K <: Symbol, V, R <: HList, T <: HList](implicit
                                                                        wit: Witness.Aux[K],
                                                                        gen: LabelledGeneric.Aux[V, R],
                                                                        tmrH: Lazy[ToMapRec[R]],
                                                                        tmrT: Lazy[ToMapRec[T]]
                                                                       ): ToMapRec[FieldType[K, Seq[V]] :: T] = new ToMapRec[FieldType[K, Seq[V]] :: T] {
      def apply(l: FieldType[K, Seq[V]] :: T): js.Dictionary[Any] = {
        val head =  l.head.map(item => tmrH.value(gen.to(item))).toJSArray
        val tail: mutable.Map[String, Any] = tmrT.value(l.tail)
        tail + (wit.value.name -> head)
      }.toJSDictionary
    }

    implicit def hconsToMapRecOption0[K <: Symbol, V, R <: HList, T <: HList](implicit
                                                                           wit: Witness.Aux[K],
                                                                           gen: LabelledGeneric.Aux[V, R],
                                                                           tmrH: Lazy[ToMapRec[R]],
                                                                           tmrT: Lazy[ToMapRec[T]]
                                                                          ): ToMapRec[FieldType[K, Option[V]] :: T] = new ToMapRec[FieldType[K, Option[V]] :: T] {
      def apply(l: FieldType[K, Option[V]] :: T): js.Dictionary[Any] = {
        val head =  l.head.map(item => tmrH.value(gen.to(item))).orUndefined
        val tail: mutable.Map[String, Any] = tmrT.value(l.tail)
        tail + (wit.value.name -> head)
      }.toJSDictionary
    }

    implicit def hconsToMapRec0[K <: Symbol, V, R <: HList, T <: HList](implicit
                                                                        wit: Witness.Aux[K],
                                                                        gen: LabelledGeneric.Aux[V, R],
                                                                        tmrH: Lazy[ToMapRec[R]],
                                                                        tmrT: Lazy[ToMapRec[T]]
                                                                       ): ToMapRec[FieldType[K, V] :: T] = new ToMapRec[FieldType[K, V] :: T] {
      def apply(l: FieldType[K, V] :: T): js.Dictionary[Any] = {
        val tail: mutable.Map[String, Any] = tmrT.value(l.tail)
        tail + (wit.value.name -> tmrH.value(gen.to(l.head)))
      }.toJSDictionary
    }



  }


  implicit class ToMapRecOps[A](val a: A) extends AnyVal {
    //def toMapRec[L <: HList](implicit gen: LabelledGeneric.Aux[A, L], tmr: Lazy[ToMapRec[L]]): Map[String, Any] = tmr.value(gen.to(a))
    def toDic[L <: HList](implicit gen: LabelledGeneric.Aux[A, L], tmr: Lazy[ToMapRec[L]]): js.Dictionary[Any]  = tmr.value(gen.to(a))
  }
}

*/

object JSDictionaries {
  implicit class ProvinceAsDict(p: Province) {
    def asObject: js.Object = js.Dynamic.literal(
      "loc" -> p.loc,
      "land" -> p.land,
      "name" -> p.name,
      "protected" -> p.`protected`,
      "race" -> p.race,
      "online" -> p.online,
      "honor" -> p.honor,
      "nw" -> p.nw
    )

  }

  implicit class KingdomAsDict(kd: Kingdom) {
    def asObject: js.Object = js.Dynamic.literal(
      "count" -> kd.count,
      "loc" -> kd.loc,
      "provinces" -> kd.provinces.map(_.asObject).toJSArray,
      "land" -> kd.land,
      "name" -> kd.name,
      "stance" -> kd.stance.asObject,
      "wars" -> kd.wars.toJSArray,
      "honor" -> kd.honor,
      "nw" -> kd.nw
    )

    def asOnlyObject: js.Object = js.Dynamic.literal(
      "count" -> kd.count,
      "loc" -> kd.loc,
      "land" -> kd.land,
      "name" -> kd.name,
      "stance" -> kd.stance.asObject,
      "wars" -> kd.wars.toJSArray,
      "honor" -> kd.honor,
      "nw" -> kd.nw
    )
  }

  implicit class StanceAsDict(s: Stance) {
    def asObject: js.Object =  js.Dynamic.literal(
      "stance" -> s.stance,
      "towards" -> s.towards.orUndefined
    )
  }

}