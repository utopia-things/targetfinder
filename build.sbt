import deployssh.DeploySSH._
import sbt.Keys._

lazy val root = project
  .in(file("."))
  .enablePlugins(DeploySSH)
  .aggregate(tfJS, tfJVM)
  .settings(
    publish := {},
    publishLocal := {}
  )


lazy val tf = crossProject
  .in(file("."))
  .enablePlugins(DeploySSH)
  .enablePlugins(JavaAppPackaging)
  .settings(
    organization := "jrd.rocks",
    name := "jrd",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.2",
    addCompilerPlugin(
      "org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full
    ),
    scalacOptions ++= Seq("-opt:l:classpath"),
    libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-parser",
      "io.circe" %%% "circe-optics"
    ).map(_ % "0.7.0"),
    libraryDependencies += "com.lihaoyi" %%% "autowire" % "0.2.6"
  )
  .jvmSettings(
    libraryDependencies ++= Seq(
      "org.http4s"     %% "http4s-blaze-server" % "0.17.0-M2",
      "org.http4s"     %% "http4s-blaze-client" % "0.17.0-M2",
      "org.http4s"     %% "http4s-circe"        % "0.17.0-M2",
      "org.http4s"     %% "http4s-dsl"          % "0.17.0-M2",
      "ch.qos.logback" % "logback-classic"      % "1.2.1"
    ),
    libraryDependencies += "org.wvlet"            %% "airframe"      % "0.13",
    libraryDependencies += "com.chuusai"          %% "shapeless"     % "2.3.2",
    libraryDependencies += "org.scala-js"         %% "scalajs-stubs" % scalaJSVersion % "provided",
    libraryDependencies += "com.github.pathikrit" %% "better-files"  % "3.0.0",
//libraryDependencies += "com.typesafe.akka"    %% "akka-actor"   % "2.5.0"

// https://mvnrepository.com/artifact/com.espertech/esper
    libraryDependencies += "com.espertech" % "esper" % "6.0.1",
    libraryDependencies ++= Seq(
      "com.orientechnologies" % "orientdb-jdbc",
      "com.orientechnologies" % "orientdb-core",
      "com.orientechnologies" % "orientdb-client"
    ).map(_ % "2.2.18"),
    libraryDependencies += "org.scalikejdbc" %% "scalikejdbc" % "2.5.1",
    //libraryDependencies += "eu.unicredit"    %% "shocon"      % "0.1.7",
    libraryDependencies += "com.typesafe" % "config" % "1.3.1",
    fork in run := true,
    javaOptions in run ++= Seq("-Xmx512m", "-XX:MaxDirectMemorySize=10g"),
    javaOptions in reStart ++= Seq("-Xmx512m", "-XX:MaxDirectMemorySize=10g"),
    mainClass in reStart := Some("targetfinder.apps.Server"),
    // deploy stuff
    mappings in (Compile, packageDoc) := Seq(),
    deployConfigs += ServerConfig("ugame", "utopia-game.info", user = Some("tf")),
    deploySshServersNames ++= Seq("ugame"),
    deployArtifacts ++= Seq(
      ArtifactSSH((stage in Universal).value, "/home/tf/tmp/app"),
      ArtifactSSH(baseDirectory.value / ".." / "client" / "dist", "/home/tf/tmp/client")
    ),
    deploySshExecBefore ++= Seq((ssh: jassh.SSH) => {
      ssh.execOnce("supervisorctl stop app")
      ssh.execOnce("rm -frd /home/tf/tmp")
      ssh.execOnce("mkdir -p /home/tf/tmp/client")
      ssh.execOnce("mkdir -p /home/tf/tmp/app")
    }),
    deploySshExecAfter ++= Seq((ssh: jassh.SSH) => {
      ssh.execOnce("chmod 0755 /home/tf/tmp/app/bin/server")
      ssh.execOnce("supervisorctl start app")
    })
  )
  .jsSettings(
    scalaJSModuleKind := ModuleKind.CommonJSModule,
    artifactPath in (Compile, fullOptJS) := baseDirectory.value / ".." / "client" / "static" / "tf.js"
  )

lazy val tfJVM = tf.jvm
lazy val tfJS  = tf.js
