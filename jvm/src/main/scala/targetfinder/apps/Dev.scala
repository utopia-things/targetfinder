package targetfinder.apps

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import com.typesafe.config.{Config, ConfigFactory}
import targetfinder.cep.Esper
import targetfinder.db.OrientDb
import targetfinder.serialization.{CirceParserDateTime, ProvinceEvent}
import targetfinder.streams.{KingdomStream}
import wvlet.airframe.{bind, _}
import wvlet.surface.tag._

object Dev extends App {

  trait App {
    val kdstream = bind[KingdomStream].onInit { x => println("a") }
      .onStart { x => println("b") }
      .onShutdown { x => println("c") }
    val db = bind[OrientDb]
    val esper = bind[Esper]
    val encoder = bind[CirceParserDateTime]
    val conf = bind[Config]

    import esper.implicits._
    //val y = bind[Y]
    //val z = bind[Z]
    // Do something with X, Y, and Z
    db.debugQuery("select * from tick")

    val mostRecent = db.querySingle("select * from Tick order by tick desc,  tick_partial desc limit 1")(rs => rs.get[Int]("tick")).map(encoder.tickToTime)

    kdstream.handle { data =>
      println(data.tick)
      //data.unixTime
      import java.time.Instant
      import java.time.LocalDateTime
      import java.time.ZoneId
      val intelTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(data.unixTime), ZoneId.systemDefault)
      if(mostRecent.fold(true)(ChronoUnit.HOURS.between(intelTime, _) < conf.getDuration("events.maxAge").toHours)) {
        // intel must be processed
        data.kds.foreach { kd =>
          kd.provinces. foreach { prov =>
            esper.engine.sendEvent(ProvinceEvent(data.tick, kd, prov))
          }
        }

      }
      db.addTick(data.tick, data.kds)
    }
    println("We got APP")

    db.debugQuery("select * from Tick order by tick desc,  tick_partial desc limit 1")


  }

  val session = design
      .bind[OrientDb].toSingleton
      .bind[Esper].toSingleton
      .newSession

  session.start
  val app : App = session.build[App]
  //Thread.sleep(6000)

}
