package targetfinder.apps

import java.time.LocalDateTime

import com.typesafe.config.{Config, ConfigFactory}
import fs2.{Stream, Task}
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.util.StreamApp
import targetfinder.apps.DevParser._
import targetfinder.cep.Esper
import targetfinder.db.OrientDb
import targetfinder.serialization.CirceParserDateTime
import targetfinder.services.BasicService
import targetfinder.streams.KingdomStream
import wvlet.airframe.{Design, newDesign}
object Server extends StreamApp {
  val session = design.bind[BasicService].toSingleton.newSession
  session.start

  val BasicService = session.build[BasicService]

  override def stream(args: List[String]): Stream[Task, Nothing] = {
    BlazeBuilder
      .bindHttp(8080, "localhost")
      .mountService(BasicService.service)
      .serve
  }
}
