package targetfinder

import java.time.LocalDateTime

import com.typesafe.config.{Config, ConfigFactory}
import targetfinder.serialization.{CirceParser, CirceParserDateTime}
import targetfinder.streams.KingdomStream
import wvlet.airframe.{Design, newDesign}

package object apps {
  val fullConf = ConfigFactory.parseString(scala.io.Source.fromInputStream(new Object {}.getClass.getResourceAsStream("/tf.conf")).getLines.mkString("\n")).resolve()

  val design: Design =
    newDesign
      .bind[LocalDateTime].toInstance(LocalDateTime.of(2017, 3, 11, 18, 0))
      .bind[KingdomStream].toSingleton
      .bind[CirceParserDateTime].toSingleton
      .bind[Config].toInstance(fullConf.getConfig(System.getProperty("env", fullConf.getString("env"))))

}
