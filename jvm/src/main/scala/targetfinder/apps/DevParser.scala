package targetfinder.apps

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import better.files.File
import com.typesafe.config.{Config, ConfigFactory}
import targetfinder.cep.Esper
import targetfinder.db.OrientDb
import targetfinder.serialization.{CirceParser, ProvinceEvent}
import targetfinder.streams.{KingdomStream}
import wvlet.airframe.{bind, _}
import wvlet.surface.tag._
import better.files._

object DevParser extends App {

  trait App {
    val encoder = bind[CirceParser]
    val conf = bind[Config]

    val dir                = conf.getString("data.jsonPath").toFile
    val data = dir.glob("*.{json}").toSeq.sortBy(_.nameWithoutExtension).last

    encoder.jsonFromString(data.contentAsString).foreach {implicit json =>
      encoder.provsLike("frog").foreach { provString =>
        //println(encoder.kdFor(provString))
        println(encoder.provFor(provString))
      }
    }

  }


  val session = design.newSession
  session.start
  val app: App = session.build[App]
  //Thread.sleep(6000)

}
