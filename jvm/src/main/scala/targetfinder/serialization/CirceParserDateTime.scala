package targetfinder.serialization

import java.time.{LocalDateTime, ZoneId}
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

import io.circe.optics.JsonPath._
import io.circe.parser._
import wvlet.airframe._

case class JsonData(tick: (Int, Int, String), unixTime: Long, kds: Seq[Kingdom])

trait CirceParserDateTime extends CirceParser {
  val gameStart = bind[LocalDateTime]

  val time = root.each.filter(_.isString).string



  def timeToTick(time: LocalDateTime): (Int, Int) = {
    val minpart = Math.round(ChronoUnit.MINUTES.between(gameStart, time)) % 60
    val hours   = Math.floor(ChronoUnit.MINUTES.between(gameStart, time) / 60)
    (hours.toInt, if ((0 to 15).contains(minpart)) {
      0
    } else if ((15 to 30).contains(minpart)) {
      1
    } else if ((30 to 45).contains(minpart)) {
      2
    } else {
      3
    })
  }

  def tickToTime(tick: Int) = {
    gameStart.plusHours(tick)
  }

  def timeToUtotime(time: LocalDateTime): String = {
    val hours      = ChronoUnit.HOURS.between(gameStart, time)
    lazy val day   = hours % 24 + 1
    lazy val month = Seq("Jan", "Feb", "Mar", "Apr", "May", "June", "July")(((hours / 24) % 7).toInt)
    lazy val year  = hours / (24 * 7)
    s"$month $day, YR$year"
  }

  def parseData(s: String): Option[JsonData] = {
    parse(s).map { json =>
      time
        .headOption(json)
        .map { t =>
          import java.time.LocalDateTime
          val tick: LocalDateTime = LocalDateTime.parse(t, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS"))
          val unixTime: Long      = tick.atZone(ZoneId.of("UTC")).toInstant.toEpochMilli
          //println(s"$t ${tick.format(DateTimeFormatter.ISO_DATE_TIME)}")
          //println(timeToUtotime(tick))
          val r = timeToTick(tick)
          ((r._1, r._2, timeToUtotime(tick)), unixTime)
        }
        .map {
          case (t, unixTime) =>
            JsonData(t, unixTime, kds.getAll(json))
        }
    }.toOption.flatten
  }
}
