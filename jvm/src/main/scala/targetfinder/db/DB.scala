package targetfinder.db

import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import com.typesafe.config.Config
import scalikejdbc._
import targetfinder.serialization.Kingdom
import wvlet.airframe.bind

import scala.io.Source

trait OrientDb {
  val conf = bind[Config]

  val dbPath                = conf.getString("data.dbPath")

  if(conf.getBoolean("data.createNew")) {
    val factory = new OrientGraphFactory(s"plocal:${dbPath}", "admin", "admin")
    try {
      val g = factory.getNoTx
    } finally {
      factory.close()
    }
  }


  Class.forName("com.orientechnologies.orient.jdbc.OrientJdbcDriver")
  ConnectionPool.singleton(s"jdbc:orient:plocal:${dbPath}", "admin", "admin")

  implicit val s = AutoSession

  def clean() = {
    Source.fromResource("db.sql").mkString.split(";").filter(!_.isEmpty).foreach(SQL(_).execute.apply())
  }

  if(conf.getBoolean("data.createNew") || conf.getBoolean("data.clean")) {
    clean()
  }


    def addTick(tick: (Int, Int, _), kds: Seq[Kingdom]): Boolean = {
      //session.toStatementExecutor()
      val id = sql"select @rid from Tick where tick = ${tick._1} and tick_partial = ${tick._2}".map(rs => rs.get[String]("@rid")).single.apply()
      if (id.isEmpty) {
        println(s"ADD TICK $tick to DB")
        sql"insert into Tick (tick, tick_partial) values (${tick._1}, ${tick._2})".execute.apply()
        kds.foreach { kd =>
          sql"insert into Kingdom (tick, tick_partial, count, loc, land, name, stance, wars_count, wars_won, honor, nw) values (${tick._1}, ${tick._2}, ${kd.count}, ${kd.loc}, ${kd.land}, ${kd.name}, ${kd.stance.stance.toLowerCase}, ${kd
            .wars(0)}, ${kd.wars(1)}, ${kd.honor}, ${kd.nw})".execute.apply()
          kd.provinces.foreach { prov =>
            sql"insert into Province (tick, tick_partial, loc, land, name, protected, race, online, honor, nw) values (${tick._1}, ${tick._2}, ${prov.loc}, ${prov.land}, ${prov.name}, ${prov.`protected`}, ${prov.race}, ${prov.online}, ${prov.honor}, ${prov.nw})".execute().apply()
          }
        }
        true
      } else {
        println(s"TICK $tick already in DB")
        false
      }
    }

    def debugQuery(q: String): Unit = {
      SQL(q).map(rs => rs.toMap()).list.apply().foreach(println(_))
    }

    def querySingle[T](q: String)(transform: WrappedResultSet => T): Option[T] = {
      SQL(q).map(transform).single.apply()
    }

}
