package targetfinder.streams

import java.time.LocalDateTime

import better.files._
import better.files.ThreadBackedFileMonitor
import com.typesafe.config.Config
import io.circe.optics.JsonPath.root
import targetfinder.serialization._
import wvlet.airframe._
import wvlet.surface.tag._

trait KingdomStream {
  val encoder = bind[CirceParserDateTime]
  val conf = bind[Config]

  val dir                = conf.getString("data.jsonPath").toFile
  val matches: Seq[File] = dir.glob("*.{json}").toSeq.sortBy(_.nameWithoutExtension).takeRight(conf.getInt("data.fileCount"))

  def handle(handler: JsonData => Unit): ThreadBackedFileMonitor = {
    var lastTick = (0, 0)
    def uniqueHandler = (data: JsonData) => {
      if (lastTick != (data.tick._1, data.tick._2)) {
        lastTick = (data.tick._1, data.tick._2)
        handler(data)
      } else {
        println("IGNORING DUPE")
      }
    }

    matches.foreach { f =>
      val r = encoder.parseData(f.contentAsString)
      r.foreach { data =>
        uniqueHandler(data)
      }
    }

    val watcher = new ThreadBackedFileMonitor(dir, recursive = false) {
      override def onCreate(file: File) = {
        encoder.parseData(file.contentAsString).foreach(uniqueHandler)
        //println(s"$file got created")
      }
      //override def onModify(file: File) = println(s"$file got modified")
      //override def onDelete(file: File) = println(s"$file got deleted")
    }
    println("j")
    watcher.start()
    watcher
  }
}
