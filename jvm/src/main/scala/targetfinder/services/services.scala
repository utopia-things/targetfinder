package targetfinder.services

import better.files._
import com.typesafe.config.Config
import fs2.Task
import org.http4s.server.middleware.{CORS, CORSConfig}
import org.http4s.{Header, HttpService, StaticFile}
import wvlet.airframe.bind
// import fs2.Task

import fs2.Strategy
// import fs2.Strategy
// import fs2.interop.cats._
// import cats._
// import cats.implicits._
// import org.http4s.Uri
import org.http4s.dsl._
import org.http4s.circe._


trait BasicService {
  val conf = bind[Config]
  val vueAppPath = conf.getString("server.vueAppPath")

  //implicit val strategy = Strategy.fromExecutionContext(scala.concurrent.ExecutionContext.Implicits.global)
  // fs2 `Async` needs an implicit `Strategy`

  // import org.http4s.client.blaze._
  // import org.http4s.client.blaze._

  //val httpClient = PooledHttp1Client()

  import scala.concurrent.duration._
  // import scala.concurrent.duration._

  val corsConfig = CORSConfig(
    anyOrigin = false,
    allowedOrigins = Some(Set("http://127.0.0.1:8088", "http://localhost:8088", "utopia-game.com", "utopia-things.gitlab.io")),
    anyMethod = false,
    allowedMethods = Some(Set("GET", "POST")),
    allowCredentials = true,
    maxAge = 1.day.toSeconds)

  val dir                = conf.getString("data.jsonPath").toFile

  val rawService = HttpService {
    case request @ GET ->  "static" /: path  =>
      StaticFile.fromFile(file"${vueAppPath}/static/${path}".toJava, Some(request))
        .map(Task.now) // This one is require to make the types match up
        .getOrElse(NotFound()) // In case the file doesn't exist
    case request @ GET -> Root     =>
      StaticFile.fromFile(file"${vueAppPath}/index.html".toJava, Some(request))
        .map(Task.now) // This one is require to make the types match up
        .getOrElse(NotFound()) // In case the file doesn't exist
    case request @ GET -> Root / "api" / "currentJson" =>
      val file =  dir.glob("*.{json}").toSeq.max(File.Order.byModificationTime)
      StaticFile.fromFile(file.toJava, Some(request))
        .map(Task.now) // This one is require to make the types match up
        .getOrElse(NotFound()).putHeaders(Header("Cache-Control", "max-age=180")) // In case the file doesn't exist
      /* slow as molasses */
      // val target = Uri.uri("http://utopia-game.com/wol/game/kingdoms_dump/?key=l1FdkNfdklAs")
      // httpClient.expect[Json](target).flatMap(Ok(_))
  }

  val service =  CORS(rawService, corsConfig)
}