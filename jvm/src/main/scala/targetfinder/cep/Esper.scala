package targetfinder.cep

import com.espertech.esper.client.{EPListenable, EPServiceProvider}
import targetfinder.serialization.ProvinceEvent
import com.espertech.esper.client.{Configuration, EPServiceProviderManager}
import com.typesafe.config.Config
import targetfinder.cep.EsperUtil._
import targetfinder.db.OrientDb
import wvlet.airframe.bind

trait Esper {
  val db = bind[OrientDb]
  val conf = bind[Config]

  object implicits  {
    implicit def EPListenable2EPListenableX(listenable: EPListenable) = new EPListenableX(listenable)

    implicit def EPServiceProvider2EPAdministrator(x: EPServiceProvider) = x.getEPAdministrator
    implicit def EPServiceProvider2EPRuntime(x: EPServiceProvider) = x.getEPRuntime()
  }

  val config = new Configuration()
  config.getEngineDefaults.getThreading.setInternalTimerEnabled(false)
  config.addEventType("ProvinceEvent", classOf[ProvinceEvent].getName)
  val esper = EPServiceProviderManager.getProvider(this.getClass.getName, config)

  //val createContext = "create context KdByLoc partition by loc from Kingdom.province"
  val createContext = "create context ProvinceEventsByProv partition by province from ProvinceEvent"
  esper.createEPL(createContext)

  /*
  esper.createEPL("""
    create table IncomingLandKd (
      fromAddress string primary key,
      toAddress string primary key,
      countIntrusion10Sec count(*),
      countIntrusion60Sec count(*)
    )""")
  */

  esper.createEPL("""
  context ProvinceEventsByProv
  create window LandOnTick.win:length(24) as ProvinceEvent
  """)

  esper.createEPL("""
  context ProvinceEventsByProv
  insert into LandOnTick select b from pattern [
    every a = ProvinceEvent -> (b = ProvinceEvent and not ProvinceEvent(tick = a.tick))
  ]
  """)
  esper createEPL("create schema ProvinceMutation as (utime string, tick int, partialTick int, deltaLand int, deltaNw int, event ProvinceEvent, previousEvent ProvinceEvent)")
  esper createEPL("create context ProvinceMutationContext partition by event.province from ProvinceMutation")
  esper.createEPL("create window ProvinceMutationWindow.win:time(1 minutes) as ProvinceMutation")

  esper.createEPL("""
  context ProvinceEventsByProv
  insert into ProvinceMutationWindow(utime, tick, partialTick, deltaLand, deltaNw, event, previousEvent) select
    event.utime,
    event.tick,
    event.partialTick,
    event.land - previousEvent.land,
    event.nw - previousEvent.nw,
    event,
    previousEvent
    from ProvinceEvent match_recognize (
       measures
         T as event,
         H as previousEvent
       pattern (H T)
       // interval 5 hours or terminated
       define
         T as (T.land != H.land or T.nw != H.nw) and (H.tick = T.tick - 1 and H.partialTick > T.partialTick or H.tick = T.tick and H.partialTick < T.partialTick)
     )
  """).addListener { (newEvents, oldEvents) =>
    //printEvents(newEvents, oldEvents)
   val e = newEvents(0)
   //println(e.getUnderlying)

  }


  /*
  These 2 work and show a stream of incoming land
   */
  /*
  val monitor = esper.createEPL("""
       context ProvinceEventsByProv
       select * from LandOnTick
       match_recognize (
          measures H.province as province, H.tick as tick, H.partialTick as partialTick, H.land as g, T.selectFrom(a => a.land) as land
          //all matches
          after match skip to current row
          pattern (H T{2,})
          // interval 5 hours or terminated
          define
            T as T.land > prev(T.land, 1)
        )
        """)

  monitor.addListener { (newEvents, oldEvents) =>
    //printEvents(newEvents, oldEvents)
    val e = newEvents(0)
    //println(e.getUnderlying)
    //println(e.get("g").asInstanceOf[Integer])
    if (e.get("g").asInstanceOf[Integer] >= 1) {
      println(s"${e.get("province")} - ${e.get("tick")} ${e.get("partialTick")} ${e.get("g")} ${e.get("land")}")
    }

    //println(s"${e.get("province")}(${e.get("kd")}) : ${e.get("preLand")} -> ${e.get("postLand")} ")

  }
  */

/*
* Works for war end
* */
  /*
 esper.createEPL("""

       select event.loc, tick, partialTick, count(distinct event.province) as ct,
        event.kd.stance, event.kd.warsCount, event.kd.warsWon, previousEvent.kd.warsCount
        from ProvinceMutationWindow
       where deltaNw < 0
       group by event.loc, event.tick, event.partialTick
       having count(distinct event.province) > 10

        """) addListener { (newEvents, oldEvents) =>
    //printEvents(newEvents, oldEvents)
    val e = newEvents(0)
    println(e.getUnderlying)
    //println(e.get("g").asInstanceOf[Integer])

   // println(s"${e.get("c")} (${e.get("l")})  ${e.get("ol")} ${e.get("nl")} ${e.get("ot")}.${e.get("opt")} ${e.get("nt")}.${e.get("npt")} ")
      //println(s"${e.get("c")}", s"${e.get("p").asInstanceOf[Array[String]].mkString(", ")}")


    //println(s"${e.get("province")}(${e.get("kd")}) : ${e.get("preLand")} -> ${e.get("postLand")} ")

  }
  */

  esper.createEPL("""
       select
        event.province,
        //event.loc,
        //tick, partialTick,
        previousEvent.utime,
        event.kd.stance,
        Math.round(deltaNw / event.nw * 1000) / 10 as nwpct,
        deltaLand,
        event.kd.honor - previousEvent.kd.honor as deltaHonor

      from ProvinceMutationWindow
          where (deltaNw < 0 or deltaLand > 0) and event.loc = "6:9"
          //where event.loc = "6:9" and event.kd.honor != previousEvent.kd.honor

      //group by event.loc, event.tick, event.partialTick having count(distinct event.province) > 10

        """) addListener { (newEvents, oldEvents) =>
    //printEvents(newEvents, oldEvents)
    val e = newEvents(0)
    val result = e.getUnderlying.asInstanceOf[java.util.Map[String, String]]
    println(result.values())
    //println(e.get("g").asInstanceOf[Integer])

    // println(s"${e.get("c")} (${e.get("l")})  ${e.get("ol")} ${e.get("nl")} ${e.get("ot")}.${e.get("opt")} ${e.get("nt")}.${e.get("npt")} ")
    //println(s"${e.get("c")}", s"${e.get("p").asInstanceOf[Array[String]].mkString(", ")}")


    //println(s"${e.get("province")}(${e.get("kd")}) : ${e.get("preLand")} -> ${e.get("postLand")} ")

  }

  def engine = esper

}
