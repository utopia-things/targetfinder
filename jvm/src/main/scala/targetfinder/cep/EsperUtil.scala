package targetfinder.cep

import com.espertech.esper.client.EventBean
import com.espertech.esper.client.EPAdministrator
import com.espertech.esper.client.UpdateListener
import com.espertech.esper.client.EPListenable
import com.espertech.esper.client.EPServiceProvider

object EsperUtil {

  def printEvents(newEvents: Array[EventBean], oldEvents: Array[EventBean]) {

    printf("\nAt: %tT ", System.currentTimeMillis())
    if (newEvents != null) {
      println("New Events:" + newEvents.size)
      newEvents.foreach { ev =>
        println("\t" + ev.getUnderlying())
      }
    }

    if (oldEvents != null) {
      println("Old Events:" + oldEvents.size)
      oldEvents.foreach { ev =>
        println("\t" + ev)
      }
    }
  }

  def sleep(millis: Int) {
    Thread.sleep(millis)
  }

  implicit def EPListenable2EPListenableX(listenable: EPListenable) =
    new EPListenableX(listenable)

  implicit def EPServiceProvider2EPAdministrator(x: EPServiceProvider) = x.getEPAdministrator
  implicit def EPServiceProvider2EPRuntime(x: EPServiceProvider) = x.getEPRuntime()

  class EPListenableX(admin: EPListenable) {
    def addListener(p: (Array[EventBean], Array[EventBean]) => Unit) {
      admin.addListener(new UpdateListener {
        def update(newEvents:Array[EventBean], oldEvents:Array[EventBean]) {
          p(newEvents, oldEvents)
        }
      })
    }
  }

}