drop class Province if exists;
drop class Kingdom if exists;
drop class Tick if exists;
drop class Event if exists;

create class Province;
create class Kingdom;
create class Tick;
create class Event;