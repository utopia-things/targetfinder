import Vue from 'vue'
import Router from 'vue-router' 
import BasicTargetfinder from '@/components/basic/Targetfinder'
import BasicProvinceList from '@/components/basic/ProvinceList'
import BasicKingdomList from '@/components/basic/KingdomList'
import FancyTargetfinder from '@/components/fancy/Targetfinder'
import FancyProvinceList from '@/components/fancy/ProvinceList'
import FancyKingdomList from '@/components/fancy/KingdomList'

Vue.use(Router) 

const basicOnly = document.getElementById('app').dataset.basicOnly ? true : false

const basicRoutes = [
    {
      path: basicOnly ? "/" : '/basic/', component: BasicTargetfinder,
      children: [
        { path: '', name: "BasicProvinceList", component: BasicProvinceList,  props: true  },
        { path: 'kingdoms', name: "BasicKingdomList", component: BasicKingdomList,  props: true  }
      ]
    }
  ]

const fancyRoutes = basicOnly ? [] : [
    {
      path: "/", component: FancyTargetfinder,
      children: [
        { path: '', name: "FancyProvinceList", component: FancyProvinceList,  props: true  },
        { path: 'kingdoms', name: "FancyKingdomList", component: FancyKingdomList,  props: true  }
      ]
    }
  ]

export default new Router({
  // mode: 'history',
  routes: basicRoutes.concat(fancyRoutes)
})
