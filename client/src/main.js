// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

Vue.filter('number', function(number){
    return new Intl.NumberFormat().format(Math.round(number))
});

const JSApi = require('@/../static/tf.js').JSApi
const jsonUrl = document.getElementById('app').dataset.jsonUrl

const basicOnly = document.getElementById('app').dataset.basicOnly ? true : false 
 
function getApi() {
    return fetch(jsonUrl).then(function (response) {
        return response.text()
    }).then(function (text) {
        return new JSApi(text)
    })
}

window.cachedApi = require('promise-memoize')(getApi, { maxAge: 3 * 60 * 1000  });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {"basicOnly": basicOnly},
  router,
  template: '<App :basicOnly="basicOnly"></App>',
  components: { App }
})
